# Jitsi Token Service
This bot is not about adapting the current jitsi integration provided by Element
IM as widget.

If you have a configured jitsi server to be authenticated with jwt tokens as in
https://github.com/jitsi/lib-jitsi-meet/blob/master/doc/tokens.md , this
bot will assist you when claiming for a jwt instead of going to jwt.io each time.

You'll need to create a user and invite you in each room where you want to create
a jitsi room url respecting the matrix power levels in the room.

Adapt the creds.example.json and copy to creds.json. jitsi iss and jitsi secret must
be the same.

When the service is running invite the bot and send message

```
!jitsi-jwt
```

Additionally you can add the roomname with 

```
!jitsi-jwt cookedroomname
```

Elsewhere will generate a random roomname.

If you are a moderator in the room (alias you have enough power levels to redact
messages), then you will get a direct message from bot with the url to jitsi
room with the jwt get parameter:

```
The claimed jitsi url for room RandomHumanReadableRoomName as moderator is:
https://jitsiurlexample.org/RandomHumanReadableRoomName?jwt=BLABLABLA.BLABLABLA.BLABLA
```

Also a message with the same url but without the jwt token will be send to the
matrix room where it is claimed:

```
The jitsi url for room RandomHumanReadableRoomName as guest is:
https://jitsiurlexample.org/RandomHumanReadableRoomName
```

If you are not a moderator you'll get a notice about forbidden action:

```
You are not moderating this room please contact a moderator to claim for a jitsi token or to promote you
```


