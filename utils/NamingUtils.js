
const projectNameGenerator = require("project-name-generator");

/**
 * Generates a human readable identifier. This should not be used for anything
 * which needs secure/cryptographic random: just a level uniquness that is offered
 * by something like Date.now().
 * @returns {string} The randomly generated ID
 */
exports.generateHumanReadableId = function() {
    return projectNameGenerator({words: 4}).raw.map(w => {
        return w[0].toUpperCase() + w.substring(1).toLowerCase();
    }).join('');
}

